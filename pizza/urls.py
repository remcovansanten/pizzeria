""" url's for the pizza app"""
from django.urls import path

from . import views

app_name = 'pizza'
urlpatterns = [
    #homepage
    path('', views.index, name = 'index'),
    #Overview page og Pizza's
    path('pizzas/', views.pizzas, name = 'pizzas'),
    #detail page of pizza
    path('pizza/<int:pizza_id>/', views.pizza, name = 'pizza'),
]
