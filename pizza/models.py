from django.db import models

# Create your models here.
class Pizza(models.Model):
    """Name of a Pizza"""
    text = models.CharField(max_length=100)
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """Return a string representation of the model.""" 
        return self.text


class Topping(models.Model):
    """ The toppings per pizza."""
    pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE)
    text = models.TextField(max_length=100)

    def __str__(self):
        """Return a string representation of the topping."""
        if len(self.text) > 50:
            return f"{self.text[:50]}..."
        else:
            return f"{self.text}"
